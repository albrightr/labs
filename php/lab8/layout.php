<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab8: Login</title>
        <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/default.css" rel="stylesheet" media="screen">
	</head>
    <body>                
        <div class="container">
        	<h1 class="title">Awesome Recipe Site</h1>        

			<?php echo $content?>
			
		</div>
        <a href="index.php?action=logout">Logout</a>
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="../lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
