<?php 
//Acts as controller, checks if you clicked on form, hit submit for thanks page, if not display list of recipes.
//Uses View and RecipeModel class
error_reporting(E_ALL);
ini_set('display_errors', 1);
	
require_once('View.php');
require_once('RecipeModel.php');
require_once('loginAuth.php');

session_start();
session_regenerate_id();

if (isset($_SESSION['loggedOn'])){
	//Check for a logout request
	if(isset($_GET['action']) &&  $_GET['action']=="logout"){
		//Reset session and unset session
		session_unset();
		unset($_SESSION['loggedOn']);
		$v = new View('login',null);
		$v->render();
	}
	elseif ($_SERVER['REQUEST_METHOD']== "POST" && $_GET['action']=="insert"){
		$rModel = new RecipeModel;
		$rModel->insert();
		$v = new View('thanks',null);
		$v->render();
	}
	elseif($_SERVER['REQUEST_METHOD']== "POST" && $_GET['action']=="delete"){
		$rModel = new RecipeModel;
		$rModel->delete();
		$v = new View('delete',null);
		$v->render();	
	}
	
	elseif (isset($_GET['action']) && $_GET['action']=="insert"){
		$v = new View('form', null);
		$v->render();
	}
	
	else {
		$rModel = new RecipeModel;
		$recipes = $rModel->findAll();
		$v = new View('list',$recipes);
		$v->render();
	}
}
elseif($_SERVER['REQUEST_METHOD']== "POST" && $_GET['action']=="login"){
		$loginAuth = new LoginAuth();
		$auth = $loginAuth->setAuth();
		if($auth==true){
			$_SESSION['loggedOn'] = 1;
		}
		$v = new View('loggedOn',$auth);
		$v->render();	
}
elseif($_SERVER['REQUEST_METHOD']== "POST" && $_GET['action']=="addUser"){
		$loginAuth = new LoginAuth();
		$auth = $loginAuth->save();
		if($auth==true){
			$_SESSION['loggedOn'] = 1;
		}
		$v = new View('loggedOn',$auth);
		$v->render();	
}
else{
		$v = new View('login',null);
		$v->render();
}


?>


