<?php
require_once('conn.php');

class LoginAuth{
	
	function initUser(){
		$conn = Database::get_connection();
		$username = $username;
		$hash = $hash;
		$query = $conn->prepare("delimiter $$
			CREATE TABLE `user` (
			  `username` varchar(50) NOT NULL,
			  `hash` char(60) NOT NULL,
			  PRIMARY KEY (`username`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1$$
			INSERT INTO user (username, hash)
			VALUES('rachel','$2a$10$abigfatcatsitsonlittlehlous7eCUQka/NFdtVvdm6aTftxU4dC');

			");
		$query->execute();
	}
	public function setAuth(){
		$auth=false;
		$conn = Database::get_connection();
		$username = htmlentities($_POST['username']);
		$password = htmlentities($_POST['password']);
		
		$query = "SELECT * FROM user WHERE username ='".$username."'";
		$result = $conn->query($query);
		if($result){
			 while ($row = $result->fetch_assoc()) {
			   $hash = $row['hash'];
			}
			$test = new PasswordHash();
			$hashTest = $test->hash_password($password, $hash);
			if($hashTest == $hash){
				
				$auth = true;
			}
			$result->free();
		}
		
		else
			$auth = false;
		 /* free result set */
		
		return $auth;
	}
	
   function save($username,$hash){
		$conn = Database::get_connection();
		$username = $username;
		$hash = $hash;
		$query = $conn->prepare("INSERT INTO user (username, hash) VALUES (?,?)");
		$query->bind_param('ss', $username, $hash);//Prevent SQL injections
		$query->execute();
	}
}
class PasswordHash{
	function hash_password($password) {
		$cost = '10';
		$salt = 'abigfatcatsitsonlittledogs';
		return crypt($password, '$2a$' .$cost . '$' . $salt . '$');
	}
}

//albrightr,apples
//rachel, oranges
//cs295,lab8
