<h2>Thank you for submitting a recipe for:<em> <?php echo htmlentities($_POST['title']);?> </em></h2>
<h3>Ingredients</h3>
	<ul>
		 <?php 
			$ingredients = $_POST['myIngredients'];
			foreach ($ingredients as $value){ ?>
                <li>
                        <?php echo $value; ?>
                </li>
        <?php } ?>
	</ul>
<h3>Instructions</h3>
	<p><?php echo htmlentities($_POST['instructions']);?>
	
	</p>
	<a href="index.php">Return to recipe list</a>  

