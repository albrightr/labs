<?php 
//Acts as controller, checks if you clicked on form, hit submit for thanks page, if not display list of recipes.
	function render($page) {
		ob_start();
		include $page.'.php';
		$content = ob_get_clean();
		include 'layout.php';
	}
	
	if ($_SERVER['REQUEST_METHOD']== "POST" && $_GET['action']=="insert")
		render('thanks');
	elseif (isset($_GET['action']) && $_GET['action']=="insert")
		render('form');
	else 
		render('list');
	
?>

