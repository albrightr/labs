<?php 
//Acts as controller, checks if you clicked on form, hit submit for thanks page, if not display list of recipes.
//Uses View and RecipeModel class
error_reporting(E_ALL);
ini_set('display_errors', 1);
	
	require('View.php');
	require ('RecipeModel.php');
	
	if ($_SERVER['REQUEST_METHOD']== "POST" && $_GET['action']=="insert"){
		$rModel = new RecipeModel;
		$rModel->insert();
		$v = new View('thanks',null);
		$v->render();}
	elseif($_SERVER['REQUEST_METHOD']== "POST" && $_GET['action']=="delete"){
		$rModel = new RecipeModel;
		$rModel->delete();
		$v = new View('delete',null);
		$v->render();	
	}
	elseif (isset($_GET['action']) && $_GET['action']=="insert"){
		$v = new View('form', null);
		$v->render();}
	
	else {
		$rModel = new RecipeModel;
		$recipes = $rModel->findAll();
		$v = new View('list',$recipes);
		$v->render();
	}
	
?>


