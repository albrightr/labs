
<a href="index.php?action=insert">Insert a recipe</a>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Title</th>                  
            <th>Instructions</th>
			<th>Ingredients</th>
			<th>Delete?</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($this->data as $recipe) { ?>     
        <tr>
			<td><?php $id =htmlentities($recipe->id); 
					echo $id;?></td>
            <td><?php echo htmlentities($recipe->title); ?></td>                                
            <td><?php echo htmlentities($recipe->instructions); ?></td>
			<td><?php foreach ($recipe->ingredients as $eachIngredient){
						echo $eachIngredient. ", ";
						}?></td>
			<td><form action="index.php?action=delete" method="post">
					<button type="submit" value="<?php echo $id;?> " name="delete"> Delete Me</button></form>
        </tr>                                
    <?php } ?>          
    </tbody>                

</table> 
</html>

