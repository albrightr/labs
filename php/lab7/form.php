
	<form action="index.php?action=insert" method="post">
	  			<fieldset>
		    		<legend>Insert a recipe</legend>
					
					<label>Title</label>
		    		<input class="input-xxlarge" type="text" placeholder="Spaghetti" name="title">
		    				    		
		    		
		    		<label>Ingredients</label>
					
					<script src="addInput.js" language="Javascript" type="text/javascript"></script>
					
		    		<div id= "dynamicIngredient">
						Ingredient 1 <br><input class="input-xlarge" type="text" placeholder="Ingredient" name="myIngredients[]">
					</div>
					<input type="button" value="Add another text input" onClick="addInput('dynamicIngredient');">
		    		
		    				    		
		    		<label>Instructions</label>
		    		<div><textarea name="instructions" class="input-block-level" rows="5"></textarea></div>		    		
		    		
		    		<button type="submit" class="btn btn-primary">Submit</button>
	  			</fieldset>
			</form>
			<a href="index.php">Return to recipe list</a>  

