var counter = 1;
var limit = 15;
function addInput(divName){
     if (counter == limit)  {
          alert("You have reached the limit of adding " + counter + " ingredients");
     }
     else {
          var newdiv = document.createElement('div');
          newdiv.innerHTML = "Ingredient " + (counter + 1) + " <br><input class='input-xlarge' type='text' placeholder='Ingredient' name='myIngredients[]'>";
          document.getElementById(divName).appendChild(newdiv);
          counter++;
     }
}
