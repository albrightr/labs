<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//For lab7 will need 3 entities: Recipe, Ingredient, Recipe_Ingred
require ('Model.php');
require_once('conn.php');

class RecipeModel extends Model{
	public function findAll(){
		$results=array();
		$conn = Database::get_connection();
		$query = "SELECT * from recipe7;";
		$res = $conn->query($query);
		while ($row = $res->fetch_assoc()) {
			$ingredients = array();
			//Select items in the ingredient table with the matching recipe id to the current row
			$recipeID =$row['id'];
			$query1 = "SELECT * FROM recipe_ingred7 as RI INNER JOIN ingredient7 as I on RI.ingredientID=I.id WHERE RI.recipeID =".$recipeID;
			$resI = $conn->query($query1);
			//Create an array of ingredients
			while($row2 = $resI->fetch_assoc()){
				$ingredients[]= $row2['name'];
			}
			$resI->free();
			
			$results[] = new Recipe( 
				$row['id'],
				$row['title'],
				$row['instructions'],
				$ingredients
				);
		} 
		$res->free();//clears stuff from memory when done with it. 
		//Error checking for empty tables
		// if($results == null)
			// $results= array(new Recipe('0', 'Demo', 'Have fun', ['flour','milk', 'baking soda', 'etc.']));
		return $results;
	}
	//$query->fetch(); Gives us one record. 
	
	
	
	
	//Use objects when possible.
	function insert(){
		//File to run insert query
		$conn = Database::get_connection();
		//Insert into Recipe table
		$title = htmlentities($_POST['title']);
		$instructions = htmlentities($_POST['instructions']);
		$query = $conn->prepare("INSERT INTO recipe7 (title, instructions) VALUES (?,?)");
		$query->bind_param('ss', $title, $instructions);//Prevent SQL injections
		$query->execute();
		//Store recipeID for recipe_ingred7 table
		$recipeID =$conn->insert_id;
		
		//Insert into Ingredient table
		$ingredients = $_POST['myIngredients'];
		//Loop through array and insert each array object into the ingredient7 table then into the intersection recipe_ingred7 table.
		foreach ($ingredients as $eachIngredient) {
			$eachIngredient = htmlentities($eachIngredient);
			$query = $conn->prepare("INSERT INTO ingredient7 (name) VALUES (?)");
			$query->bind_param('s', $eachIngredient);//Prevent SQL injections
			$query->execute();
			//Insert into REcipe_ingred table
			$id = $conn->insert_id;//Need this to insert a row into the recipe_ingred table
			$query = $conn->prepare("INSERT INTO recipe_ingred7 (recipeID, ingredientID) VALUES (?,?)");
			$query->bind_param('ss', $recipeID, $id);//Prevent SQL injections
			$query->execute();
		}
		
	}
	function delete(){
		$conn = Database::get_connection();
			$ingredients = array();
			//Select items in the ingredient table with the matching recipe id to the current row
			$recipeID =$_POST['delete'];
			$query1 = "SELECT * FROM recipe_ingred7 as RI INNER JOIN ingredient7 as I on RI.ingredientID=I.id WHERE RI.recipeID =".$recipeID;
			$resI = $conn->query($query1);
			//Create an array of ingredients
			while($row2 = $resI->fetch_assoc()){
				$ingredients[]= $row2['id'];
			}
		//Delete array associated with the recipe.
		foreach ($ingredients as $eachIngredient) {
			$eachIngredient = htmlentities($eachIngredient);
			$query = $conn->prepare("DELETE FROM ingredient7 WHERE id = ?");
			$query->bind_param('s', $eachIngredient);//Prevent SQL injections
			$query->execute();
		}
		//Delete from Recipe table
		$query = $conn->prepare("DELETE FROM recipe7 WHERE id= ?");
		$query->bind_param('s', $recipeID);//Prevent SQL injections
		$query->execute();
		$resI->free();
		//Deleting from both table will auto delete records in the intersection table. 
		
	}//end of delete function
	
}//end of RecipeModel class
class Recipe{
	public $id;
	public $title;
	public $instructions;
	public $ingredients=array();
	
	function __construct( $id, $title, $instructions, $ingredients){
		$this->id= $id;
		$this->title= $title;
		$this->instructions= $instructions;
		$this->ingredients = $ingredients;
	}
}