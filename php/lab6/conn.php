<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
class Database {
private static $conn = NULL;



static function get_connection()
    {
	
		if (isset($_ENV['OPENSHIFT_APP_NAME'])) {
			define("DB_NAME", "labs");
			define("DB_HOST", $_ENV['OPENSHIFT_MYSQL_DB_HOST']);
			define("DB_USER", $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
			define("DB_PASS", $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);    
		} else {
			define("DB_NAME", "labs");
			define("DB_HOST", "localhost");
			define("DB_USER", "root");
			define("DB_PASS", "");  
		}
        if (self::$conn == NULL) {                  
            self::$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
            if (mysqli_connect_errno()){
                echo "You got problems.";
				die;
            }
        }

        return self::$conn;
    }
}

