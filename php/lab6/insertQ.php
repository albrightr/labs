<?php
//File to run insert query
include_once 'conn.php';
$conn = Database::get_connection();

$ititle = "spaghetti";
$instructions = "Work it!";

//This goes in Model.php
function insertRecipe($title, $instructions){
	$query = $conn->prepare("INSERT INTO recipe (title,instructions) VALUES (?,?)";
	$query->bind_param('ss', $title, $instructions);
	$query->execute();
}