<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require ('Model.php');
include_once('conn.php');


class RecipeModel extends Model{
	public function findAll(){
		$conn = Database::get_connection();
		$query = "SELECT * from recipe";
		$res = $conn->query($query);
		
		while ($row = $res->fetch_assoc()) {
			$results[] = new Recipe( 
				$row['id'],
				$row['title'],
				$row['ingredient0'],
				$row['ingredient1'],
				$row['ingredient2'],
				$row['instructions']
				);
		} 
		$res->free();//clears stuff from memory when done with it. 
		return $results;
	}
	
	function insert(){
		//File to run insert query
		$conn = Database::get_connection();
			$title = htmlentities($_POST['title']);
			$ingredient0 = htmlentities($_POST['ingredient0']);
			$ingredient1 = htmlentities($_POST['ingredient1']);
			$ingredient2 = htmlentities($_POST['ingredient2']);
			$instructions = htmlentities($_POST['instructions']);
		
		$query = $conn->prepare("INSERT INTO recipe (title, ingredient0, ingredient1, ingredient2, instructions) VALUES (?,?,?,?,?)");
		$query->bind_param('sssss', $title, $ingredient0, $ingredient1, $ingredient2, $instructions);//Prevent SQL injections
		$query->execute();
	}
	
}

class Recipe{
	public $id;
	public $title;
	public $ingredient0;
	public $ingredient1;
	public $ingredient2;
	public $instructions;
	
function __construct($id,$title,$ingredient0,$ingredient1,$ingredient2,$instructions){
		$this->id= $id;
		$this->title= $title;
		$this->ingredient0= $ingredient0;
		$this->ingredient1= $ingredient1;
		$this->ingredient2= $ingredient2;
		$this->instructions= $instructions;
	}
}