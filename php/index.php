<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Labs</title>
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/default.css" rel="stylesheet" media="screen">
	</head>
    <body>                
        <div class="container">
        	<h1 class="title">Albright's CS295 Labs</h1>        

			<table class="table table-striped">
    <thead>
        <tr>
            <th>Lab#</th>
            <th>Topic</th>                  

        </tr>
    </thead>
    <tbody> 
        <tr>
            <td>1</td><td>Development Environment</td></tr>
		<tr>
			<td>2</td><td><a href="http://labs-albrightrcs295.rhcloud.com/lab2">Forms</a></td></tr>
		<tr>
			<td>3</td><td><a href="http://labs-albrightrcs295.rhcloud.com/lab3">Single-Form</a></td></tr>
		<tr>
			<td>4</td><td><a href="http://labs-albrightrcs295.rhcloud.com/lab4">Seperating Views</a></td></tr>
		<tr>
			<td>5</td><td><a href="http://labs-albrightrcs295.rhcloud.com/lab5">Model - Control - View</a></td></tr>   
		<tr>
			<td>6</td><td><a href="http://labs-albrightrcs295.rhcloud.com/lab6">MySQL Integration w/PHP</a></td></tr>   
		<tr>
			<td>7</td><td><a href="http://labs-albrightrcs295.rhcloud.com/lab7">Multiple Ingredients</a></td></tr>   
        <tr>
			<td>8</td><td><a href="http://labs-albrightrcs295.rhcloud.com/lab8">Sessions</a></td></tr>   
    </tbody>                
			
		</div>
        
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
